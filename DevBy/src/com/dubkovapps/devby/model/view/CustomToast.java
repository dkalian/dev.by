package com.dubkovapps.devby.model.view;

import android.content.Context;
import android.view.View;
import android.widget.Toast;

import com.dubkovapps.devby.R;

public class CustomToast {
	public static Toast getSuccessToast(Context context, String message, int duration) {
		Toast toast = Toast.makeText(context, message, duration);
		View view = toast.getView();
		view.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.green_toast));
		return toast;
	}

	public static Toast getErrorToast(Context context, String message,int duration) {
		Toast toast = Toast.makeText(context, message, duration);
		View view = toast.getView();
		view.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.red_toast));
		return toast;
	}
	
	public static Toast getInfoToast(Context context, String message,int duration) { 
		Toast toast = Toast.makeText(context, message, duration);
		View view = toast.getView();
		view.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.blue_toast));
		return toast;
	}
}
