package com.dubkovapps.devby.model;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.dubkovapps.devby.AutoSearchResultsActivity;
import com.dubkovapps.devby.R;
import com.dubkovapps.devby.model.constant.Constant;

public class CustomNotification {

	String LogTag = "Custom Notification: ";

	/**
	 * @param context
	 *            Context
	 * @param StatusBarText
	 *            Text in status bar
	 * @param Title
	 *            Notification title
	 * @param Message
	 *            Notification message
	 * @param nm
	 *            Notification Mannager
	 */
	@SuppressWarnings("deprecation")
	public static void ShowNotification(Context context, String StatusBarText,
			String Title, String Message, NotificationManager nm) {
		Notification notification = new Notification(R.drawable.ic_launcher,
				StatusBarText, System.currentTimeMillis());
		Intent intent = new Intent(context, AutoSearchResultsActivity.class);
		PendingIntent pIntent = PendingIntent.getActivity(context,
				111, intent, 0);
		notification.setLatestEventInfo(context, Title, Message, pIntent);
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		nm.notify(1, notification);
	}
}