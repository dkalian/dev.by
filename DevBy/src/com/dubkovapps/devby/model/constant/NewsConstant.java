package com.dubkovapps.devby.model.constant;

public class NewsConstant {
	//*** SUB NODES "<item> node"
	public static final String NEWS_TITLE = "title";
	public static final String NEWS_PUB_DATE= "pubdate";
	public static final String NEWS_DESCRIPTION = "description";
	public static final String NEWS_LINK = "guid";
	public static final String NEWS_PICTURE = "picture";
	public static final String NEWS_NEW_TEXT = "new_text";
}
