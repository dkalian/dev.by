package com.dubkovapps.devby.model.constant;

public class SharedPreferencesConstant {
	public static final String PREFERENCES_NAME = "devBy_SP";
	public static final String PREFERENCE_LAST_NEWS_UPDATE = "lastNewsUpdate";
	public static final String PREFERENCE_LAST_COMPANIES_UPDATE = "lastCompaniesUpdate";
	public static final String PREFERENCE_LAST_JOB_UPDATE = "lastJobUpdate";
	
	public static final String PREFERENCE_FIRST_START = "firstStart";
}
