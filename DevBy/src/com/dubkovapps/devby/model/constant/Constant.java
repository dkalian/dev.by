package com.dubkovapps.devby.model.constant;

public class Constant {
	// ************** NEWS PARSER
	public static final String DEV_BY_RSS_URL = "http://dev.by/rss";

	public static final String DEV_BY_URL = "http://dev.by";

	public static final String EXTRA_NEWS = "news";

	public static final String EXTRA_NEW_DATA = "new_data";

	public static final String EXTRA_COMPANY_VACANCIES_LIST = "company_vacancies_list";
	public static final String EXTRA_VACANCY_DATA = "vacancy_data";
	
	public static final String EXTRA_COMPANY_ID = "company_id";
	public static final String EXTRA_COMPANY_TITLE = "company_title";

	public static final String API_DEV_BY_ALL_COMPANIES_URL = "http://api.dev.by/companies.json";
	public static final String API_DEV_BY_ALL_JOBS_URL = "http://api.dev.by/jobs.json";
	public static final String API_DEV_BY_COMPANY_BY_ID_URL = "http://api.dev.by/companies/";
	public static final String API_DEV_BY_VACACNY_URL = "http://api.dev.by/job/";
	
	
}
