package com.dubkovapps.devby.model.service;

import java.util.concurrent.Executors;

import com.dubkovapps.devby.model.connection.NetworkManager;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.sax.StartElementListener;
import android.util.Log;
import android.widget.Toast;

public class ServiceRepeater extends BroadcastReceiver {

	/**
	 * Empty constructor
	 */
	public ServiceRepeater() {
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		if (NetworkManager.isNetworkConnectionAvailable(context)) {
			context.startService(new Intent(context, Autosearch.class));
		}
	}
}