package com.dubkovapps.devby.model.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class AutostartReciever extends BroadcastReceiver {
	private SharedPreferences sp;

	@Override
	public void onReceive(Context context, Intent intent) {
		sp = PreferenceManager.getDefaultSharedPreferences(context);
		boolean autostart = sp.getBoolean("autostart", false);
		if (autostart == true) {
			context.startService(new Intent(context, Autosearch.class));
		}
	}
}
