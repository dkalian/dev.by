package com.dubkovapps.devby.model.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.text.format.DateUtils;

import com.dubkovapps.devby.model.CustomNotification;
import com.dubkovapps.devby.model.HTTP.PrCyClient;
import com.dubkovapps.devby.model.connection.NetworkManager;
import com.dubkovapps.devby.model.constant.Constant;
import com.dubkovapps.devby.model.dboperation.DBConstant;
import com.dubkovapps.devby.model.dboperation.DBOperations;

public class Autosearch extends Service {

	String vacancyName = "android";

	public int onStartCommand(Intent intent, int flags, int startId) {
		if (NetworkManager.isNetworkConnectionAvailable(this)) {
			someTask();
		}
		return super.onStartCommand(intent, flags, startId);
	}

	private void someTask() {
		new JobParserThread().execute();
	}

	public void onDestroy() {
		super.onDestroy();
	}

	public IBinder onBind(Intent intent) {
		return null;
	}

	ArrayList<HashMap<String, String>> newVacancies;

	public class JobParserThread extends AsyncTask<Void, Void, String> {

		final String LOG_TAG = "AutoSearch";

		boolean error = false;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		InputStream inputStream = null;
		private HttpClient client;
		private HttpGet httpGet;
		private BufferedReader in;

		@Override
		protected String doInBackground(Void... params) {
			System.out.println("STARTED!");
			SharedPreferences sp = PreferenceManager
					.getDefaultSharedPreferences(Autosearch.this);
			vacancyName = sp.getString("vacancy_name", "");
			client = PrCyClient.getInstance().client;
			httpGet = new HttpGet(Constant.API_DEV_BY_ALL_JOBS_URL);
			try {
				HttpResponse response = client.execute(httpGet);
				in = new BufferedReader(new InputStreamReader(response
						.getEntity().getContent()));
				StringBuffer sb = new StringBuffer("");
				String line = "";
				while ((line = in.readLine()) != null) {
					sb.append(line + "\n");
				}
				in.close();
				String result = sb.toString();
				JSONArray jobs = new JSONArray(result);

				ArrayList<HashMap<String, String>> vacancies = new ArrayList<HashMap<String, String>>();

				String lastUpdated = DateUtils.formatDateTime(Autosearch.this,
						System.currentTimeMillis(), DateUtils.FORMAT_SHOW_TIME
								| DateUtils.FORMAT_SHOW_DATE
								| DateUtils.FORMAT_ABBREV_ALL);

				for (int i = 0; i < jobs.length(); i++) {
					JSONObject job = jobs.getJSONObject(i).getJSONObject("job");
					HashMap<String, String> vacancy = new HashMap<String, String>();
					if (job.getString(DBConstant.ABOUT_VACANCIES_TITLE)
							.toUpperCase().contains(vacancyName.toUpperCase())) {
						vacancy.put(DBConstant.ABOUT_VACANCIES_ID,
								job.getString(DBConstant.ABOUT_VACANCIES_ID));
						vacancy.put(DBConstant.ABOUT_VACANCIES_TITLE,
								job.getString(DBConstant.ABOUT_VACANCIES_TITLE));
						vacancy.put(DBConstant.ABOUT_VACANCIES_OVERVIEW, job
								.getString(DBConstant.ABOUT_VACANCIES_OVERVIEW));
						vacancy.put(DBConstant.ABOUT_VACANCIES_SALARY, job
								.getString(DBConstant.ABOUT_VACANCIES_SALARY));
						vacancy.put(
								DBConstant.ABOUT_VACANCIES_OPENED_COMPANY_ID,
								job.getString("company_id"));
						vacancy.put(
								DBConstant.ABOUT_VACANCIES_LAST_UPDATE_DATE,
								lastUpdated);
						vacancies.add(vacancy);
					}
				}
				newVacancies = DBOperations.saveInfoAboutVacancies(
						Autosearch.this, vacancies);
				DBOperations.saveNewVacancies(Autosearch.this, newVacancies);
				setAlarmManager();
				if (newVacancies.size() != 0) {
					CustomNotification
							.ShowNotification(
									Autosearch.this,
									"Найдена вакансия",
									"Новая вакансия",
									"Ннайдено новых " + newVacancies.size()
											+ " вакансии",
									(NotificationManager) getSystemService(NOTIFICATION_SERVICE));
				}

			} catch (JSONException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (Exception e) {
				// TODO: handle exception
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
		}
	}

	private void setAlarmManager() {
		Intent intent = new Intent(this, ServiceRepeater.class);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0,
				intent, 0);
		AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(this);
		if (sp.getBoolean("auto_search", false)) {
			System.out.println("Srtting alarm!");
			alarmManager.set(AlarmManager.RTC_WAKEUP,
					 System.currentTimeMillis() + Integer.parseInt(sp.getString("interval", String.valueOf(30)))*60000, pendingIntent);
		}
	}
}
