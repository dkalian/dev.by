package com.dubkovapps.devby.model.dboperation;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DAO extends SQLiteOpenHelper {

	public DAO(Context context) {
		super(context, DBConstant.DB_NAME, null, 1);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("create table " + DBConstant.DB_NEWS_TABLE_NAME + " ("
				+ "id integer primary key autoincrement,"
				+ DBConstant.NEWS_TITLE + " text, " + DBConstant.NEWS_NEW_TEXT
				+ " text," + DBConstant.NEWS_PUB_DATE + " text,"
				+ DBConstant.NEWS_LINK + "  text, " + DBConstant.NEWS_PICTURE
				+ " text);");

		db.execSQL("create table " + DBConstant.DB_COMPANIES_TABLE_NAME + " ("
				+ DBConstant.COMPANIES_ID + " integer, "
				+ DBConstant.COMPANIES_TITLE + " text, "
				+ DBConstant.COMPANIES_REALNAME + " text, "
				+ DBConstant.COMPANIES_SUBDOMAIN + " text);");

		db.execSQL("create table " + DBConstant.DB_ABOUT_COMPANIES_TABLE_NAME
				+ " (" + DBConstant.ABOUT_COMPANIES_ID + " integer, "
				+ DBConstant.ABOUT_COMPANIES_TITLE + " text, "
				+ DBConstant.ABOUT_COMPANIES_REALNAME + " text, "
				+ DBConstant.ABOUT_COMPANIES_SUBDOMAIN + " text, "
				+ DBConstant.ABOUT_COMPANIES_VACANCIES_OPENED + " text, "
				+ DBConstant.ABOUT_COMPANIES_SUMMARY_RATING + " text, "
				+ DBConstant.ABOUT_COMPANIES_LAST_UPDATE_DATE + " text, "
				+ DBConstant.ABOUT_COMPANIES_OVERVIEW + " text);");

		db.execSQL("create table " + DBConstant.DB_VACANCIES_TABLE_NAME + " ("
				+ DBConstant.VACANCY_ID + " integer, "
				+ DBConstant.VACANCY_TITLE + " text);");

		db.execSQL("create table " + DBConstant.DB_ABOUT_VACANCIES_TABLE_NAME
				+ " (" + DBConstant.ABOUT_VACANCIES_ID + " integer, "
				+ DBConstant.ABOUT_VACANCIES_TITLE + " text, "
				+ DBConstant.ABOUT_VACANCIES_OPENED_COMPANY_ID + " integer, "
				+ DBConstant.ABOUT_VACANCIES_OVERVIEW + " text, "
				+ DBConstant.ABOUT_VACANCIES_LAST_UPDATE_DATE + " text, "
				+ DBConstant.ABOUT_VACANCIES_SALARY + " text);");
		
		db.execSQL("create table " + DBConstant.DB_NEW_VCANCIES_TABLE_NAME
				+ " (" + DBConstant.ABOUT_VACANCIES_ID + " integer, "
				+ DBConstant.ABOUT_VACANCIES_TITLE + " text, "
				+ DBConstant.ABOUT_VACANCIES_OPENED_COMPANY_ID + " integer, "
				+ DBConstant.ABOUT_VACANCIES_OVERVIEW + " text, "
				+ DBConstant.ABOUT_VACANCIES_SALARY + " text);");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}
}
