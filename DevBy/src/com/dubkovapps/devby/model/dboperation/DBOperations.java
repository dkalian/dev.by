package com.dubkovapps.devby.model.dboperation;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.dubkovapps.devby.model.constant.Constant;
import com.dubkovapps.devby.model.constant.NewsConstant;

public class DBOperations {

	final static String LOG_TAG = "Data Base Operation: ";

	public static void deleteAllData(Context context, String tableName) {
		try {
			DAO dao = new DAO(context);
			SQLiteDatabase db = dao.getWritableDatabase();
			db.delete(tableName, null, null);
			db.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void addRecords(Context context,
			ArrayList<HashMap<String, String>> data) {
		DAO dao = new DAO(context);

		SQLiteDatabase db = dao.getWritableDatabase();

		for (HashMap<String, String> record : data) {
			ContentValues cv = new ContentValues();
			try {
				// Log.d(LOG_TAG, "Добавление в CV...");
				cv.put(DBConstant.NEWS_TITLE,
						record.get(NewsConstant.NEWS_TITLE));
				cv.put(DBConstant.NEWS_NEW_TEXT,
						record.get(NewsConstant.NEWS_NEW_TEXT));
				cv.put(DBConstant.NEWS_PUB_DATE,
						record.get(NewsConstant.NEWS_PUB_DATE));
				cv.put(DBConstant.NEWS_LINK, record.get(NewsConstant.NEWS_LINK));
				cv.put(DBConstant.NEWS_PICTURE,
						record.get(NewsConstant.NEWS_PICTURE));
				// Log.d(LOG_TAG, "Выполнено");
			} catch (Exception e) {
				Log.e(LOG_TAG, "Ошибка!");
				e.printStackTrace();
			}

			try {
				// Log.d(LOG_TAG, "Вставляем в БД запись...");
				db.insertOrThrow(DBConstant.DB_NEWS_TABLE_NAME, null, cv);
				// Log.d(LOG_TAG, "Выполнено");
			} catch (Exception e) {
				// Log.e(LOG_TAG, "Ошибка!");
				e.printStackTrace();
			}
			cv.clear();
		}

		db.close();
	}

	public static ArrayList<HashMap<String, String>> getAllRecords(
			Context context) {
		ArrayList<HashMap<String, String>> data = new ArrayList<HashMap<String, String>>();
		DAO dao = new DAO(context);
		SQLiteDatabase db = dao.getReadableDatabase();
		Cursor c = db.query(DBConstant.DB_NEWS_TABLE_NAME, null, null, null,
				null, null, null);
		if (c.moveToFirst()) {
			do {

				int idf = c.getColumnIndex("id");

				int fTitle = c.getColumnIndex(DBConstant.NEWS_TITLE);
				int fText = c.getColumnIndex(DBConstant.NEWS_NEW_TEXT);
				int fPubDate = c.getColumnIndex(DBConstant.NEWS_PUB_DATE);
				int fLink = c.getColumnIndex(DBConstant.NEWS_LINK);
				int fPicture = c.getColumnIndex(DBConstant.NEWS_PICTURE);

				String id = c.getString(idf);
				String title = c.getString(fTitle);
				String text = c.getString(fText);
				String pubDate = c.getString(fPubDate);
				String link = c.getString(fLink);
				String picture = c.getString(fPicture);

				HashMap<String, String> temp = new HashMap<String, String>();

				temp.put(NewsConstant.NEWS_TITLE, title);
				temp.put(NewsConstant.NEWS_NEW_TEXT, text);
				temp.put(NewsConstant.NEWS_PUB_DATE, pubDate);
				temp.put(NewsConstant.NEWS_LINK, link);
				temp.put(NewsConstant.NEWS_PICTURE, picture);
				temp.put(DBConstant.NEWS_ID, id);

				data.add(temp);
			} while (c.moveToNext());
		} else {
			// No data
		}
		db.close();
		return data;
	}

	public static void addCompany(Context context, ContentValues data) {
		DAO dao = new DAO(context);
		SQLiteDatabase db = dao.getWritableDatabase();
		try {
			// Log.d(LOG_TAG, "Вставляем в БД запись...");
			db.insertOrThrow(DBConstant.DB_COMPANIES_TABLE_NAME, null, data);
			// Log.d(LOG_TAG, "Выполнено");
		} catch (Exception e) {
			Log.e(LOG_TAG, "Ошибка!");
			e.printStackTrace();
		}
		db.close();
	}

	public static ArrayList<HashMap<String, String>> getAllCompanies(
			Context context) {
		ArrayList<HashMap<String, String>> companiesList = new ArrayList<HashMap<String, String>>();
		DAO dao = new DAO(context);
		SQLiteDatabase db = dao.getReadableDatabase();
		Cursor c = db.query(DBConstant.DB_COMPANIES_TABLE_NAME, null, null,
				null, null, null, null);
		if (c.moveToFirst()) {
			do {
				int idf = c.getColumnIndex(DBConstant.COMPANIES_ID);
				int fTitle = c.getColumnIndex(DBConstant.COMPANIES_TITLE);
				int fRealname = c.getColumnIndex(DBConstant.COMPANIES_REALNAME);
				int fSubDomain = c
						.getColumnIndex(DBConstant.COMPANIES_SUBDOMAIN);

				String id = c.getString(idf);
				String title = c.getString(fTitle);
				String realname = c.getString(fRealname);
				String subdomain = c.getString(fSubDomain);

				HashMap<String, String> temp = new HashMap<String, String>();

				temp.put(DBConstant.COMPANIES_ID, id);
				temp.put(DBConstant.COMPANIES_TITLE, title);
				temp.put(DBConstant.COMPANIES_REALNAME, realname);
				temp.put(DBConstant.COMPANIES_SUBDOMAIN, subdomain);

				companiesList.add(temp);
			} while (c.moveToNext());
		} else {
			// No data
		}
		db.close();
		return companiesList;
	}

	public static void saveInfoAboutCompany(Context context, ContentValues cv,
			String companyID) {
		DAO dao = new DAO(context);
		SQLiteDatabase db = dao.getWritableDatabase();
		Cursor c = db.query(DBConstant.DB_ABOUT_COMPANIES_TABLE_NAME, null,
				DBConstant.ABOUT_COMPANIES_ID + " = " + companyID, null, null,
				null, null);
		if (c.moveToFirst()) {
			db.update(DBConstant.DB_ABOUT_COMPANIES_TABLE_NAME, cv,
					DBConstant.ABOUT_COMPANIES_ID + " = " + companyID, null);
		} else {
			try {
				db.insertOrThrow(DBConstant.DB_ABOUT_COMPANIES_TABLE_NAME,
						null, cv);
			} catch (Exception e) {
				Log.e(LOG_TAG, "Ошибка при добавлении подробной информации!");
				e.printStackTrace();
			}

		}
		db.close();
	}

	public static HashMap<String, String> getCompanyInfoById(Context context,
			String companyID) {
		HashMap<String, String> companyInfo = null;
		DAO dao = new DAO(context);
		SQLiteDatabase db = dao.getWritableDatabase();
		Cursor c = db.query(DBConstant.DB_ABOUT_COMPANIES_TABLE_NAME, null,
				DBConstant.ABOUT_COMPANIES_ID + " = " + companyID, null, null,
				null, null);
		if (c.moveToFirst()) {

			companyInfo = new HashMap<String, String>();

			int idf = c.getColumnIndex(DBConstant.ABOUT_COMPANIES_ID);
			int fTitle = c.getColumnIndex(DBConstant.ABOUT_COMPANIES_TITLE);
			int fRealname = c
					.getColumnIndex(DBConstant.ABOUT_COMPANIES_REALNAME);
			int fSubDomain = c
					.getColumnIndex(DBConstant.ABOUT_COMPANIES_SUBDOMAIN);
			int fSummaryRating = c
					.getColumnIndex(DBConstant.ABOUT_COMPANIES_SUMMARY_RATING);
			int fOverview = c
					.getColumnIndex(DBConstant.ABOUT_COMPANIES_OVERVIEW);
			int fLastUpdated = c
					.getColumnIndex(DBConstant.ABOUT_COMPANIES_LAST_UPDATE_DATE);
			int fVacanciesOpened = c
					.getColumnIndex(DBConstant.ABOUT_COMPANIES_VACANCIES_OPENED);

			String id = c.getString(idf);
			String title = c.getString(fTitle);
			String realname = c.getString(fRealname);
			String subdomain = c.getString(fSubDomain);
			String summaryRating = c.getString(fSummaryRating);
			String overview = c.getString(fOverview);
			String lastUpdated = c.getString(fLastUpdated);
			String vacanciesOpened = c.getString(fVacanciesOpened);

			companyInfo.put(DBConstant.ABOUT_COMPANIES_ID, id);
			companyInfo.put(DBConstant.ABOUT_COMPANIES_TITLE, title);
			companyInfo.put(DBConstant.ABOUT_COMPANIES_REALNAME, realname);
			companyInfo.put(DBConstant.ABOUT_COMPANIES_SUBDOMAIN, subdomain);
			companyInfo.put(DBConstant.ABOUT_COMPANIES_SUMMARY_RATING,
					summaryRating);
			companyInfo.put(DBConstant.ABOUT_COMPANIES_OVERVIEW, overview);
			companyInfo.put(DBConstant.ABOUT_COMPANIES_LAST_UPDATE_DATE,
					lastUpdated);
			companyInfo.put(DBConstant.ABOUT_COMPANIES_VACANCIES_OPENED,
					vacanciesOpened);
		}
		db.close();
		return companyInfo;
	}

	// ************************ VACANCIES *******************

	public static ArrayList<HashMap<String, String>> saveInfoAboutVacancies(
			Context context, ArrayList<HashMap<String, String>> vacancies) {
		ArrayList<HashMap<String, String>> newVacancies = new ArrayList<HashMap<String, String>>();
		DAO dao = new DAO(context);
		SQLiteDatabase db = dao.getWritableDatabase();
		try {
			for (HashMap<String, String> vacancy : vacancies) {
				ContentValues data = new ContentValues();

				data.put(DBConstant.ABOUT_VACANCIES_ID,
						vacancy.get(DBConstant.ABOUT_VACANCIES_ID));
				data.put(DBConstant.ABOUT_VACANCIES_OVERVIEW,
						vacancy.get(DBConstant.ABOUT_VACANCIES_OVERVIEW));
				data.put(DBConstant.ABOUT_VACANCIES_SALARY,
						vacancy.get(DBConstant.ABOUT_VACANCIES_SALARY));
				data.put(DBConstant.ABOUT_VACANCIES_TITLE,
						vacancy.get(DBConstant.ABOUT_VACANCIES_TITLE));
				data.put(DBConstant.ABOUT_VACANCIES_OPENED_COMPANY_ID, vacancy
						.get(DBConstant.ABOUT_VACANCIES_OPENED_COMPANY_ID));
				data.put(DBConstant.ABOUT_VACANCIES_LAST_UPDATE_DATE, vacancy
						.get(DBConstant.ABOUT_VACANCIES_LAST_UPDATE_DATE));

				if (db.update(
						DBConstant.DB_ABOUT_VACANCIES_TABLE_NAME,
						data,
						DBConstant.VACANCY_ID + " = "
								+ vacancy.get(DBConstant.VACANCY_ID), null) == 0) {
					db.insertOrThrow(DBConstant.DB_ABOUT_VACANCIES_TABLE_NAME,
							null, data);
					newVacancies.add(vacancy);
					Log.d(LOG_TAG, "Vacancies inserted");
				} else {
					Log.d(LOG_TAG, "Vacancies updated");
				}
			}
		} catch (Exception e) {
			Log.e(LOG_TAG, "Ошибка!");
			e.printStackTrace();
		}
		db.close();
		return newVacancies;
	}

	public static ArrayList<HashMap<String, String>> getAllCompanyVacancies(
			Context context, String companyID) {
		ArrayList<HashMap<String, String>> companyVacanciesList = new ArrayList<HashMap<String, String>>();
		DAO dao = new DAO(context);
		SQLiteDatabase db = dao.getReadableDatabase();
		Cursor c = db.query(DBConstant.DB_ABOUT_VACANCIES_TABLE_NAME, null,
				DBConstant.ABOUT_VACANCIES_OPENED_COMPANY_ID + " = "
						+ companyID, null, null, null, null);
		if (c.moveToFirst()) {
			do {
				int idf = c.getColumnIndex(DBConstant.ABOUT_VACANCIES_ID);
				int fTitle = c.getColumnIndex(DBConstant.ABOUT_VACANCIES_TITLE);

				String id = c.getString(idf);
				String title = c.getString(fTitle);

				HashMap<String, String> temp = new HashMap<String, String>();

				temp.put(DBConstant.COMPANIES_ID, id);
				temp.put(DBConstant.COMPANIES_TITLE, title);

				companyVacanciesList.add(temp);
			} while (c.moveToNext());
		} else {
			// No data
		}
		db.close();
		return companyVacanciesList;
	}

	public static ArrayList<HashMap<String, String>> getAllVacancies(
			Context context) {
		ArrayList<HashMap<String, String>> companyVacanciesList = new ArrayList<HashMap<String, String>>();
		DAO dao = new DAO(context);
		SQLiteDatabase db = dao.getReadableDatabase();
		Cursor c = db.query(DBConstant.DB_ABOUT_VACANCIES_TABLE_NAME, null,
				null, null, null, null, null);
		if (c.moveToFirst()) {
			do {
				int idf = c.getColumnIndex(DBConstant.ABOUT_VACANCIES_ID);
				int fTitle = c.getColumnIndex(DBConstant.ABOUT_VACANCIES_TITLE);

				String id = c.getString(idf);
				String title = c.getString(fTitle);

				HashMap<String, String> temp = new HashMap<String, String>();

				temp.put(DBConstant.COMPANIES_ID, id);
				temp.put(DBConstant.COMPANIES_TITLE, title);

				companyVacanciesList.add(temp);
			} while (c.moveToNext());
		} else {
			// No data
		}
		db.close();
		return companyVacanciesList;
	}

	public static HashMap<String, String> getVacacyInfo(Context context,
			String vacancyID) {
		DAO dao = new DAO(context);
		SQLiteDatabase db = dao.getReadableDatabase();
		HashMap<String, String> info = new HashMap<String, String>();
		Cursor c = db.query(DBConstant.DB_ABOUT_VACANCIES_TABLE_NAME, null,
				DBConstant.ABOUT_VACANCIES_ID + " = " + vacancyID, null, null,
				null, null);
		if (c.moveToFirst()) {
			do {
				int idf = c.getColumnIndex(DBConstant.ABOUT_VACANCIES_ID);
				int fTitle = c.getColumnIndex(DBConstant.ABOUT_VACANCIES_TITLE);
				int fOverview = c
						.getColumnIndex(DBConstant.ABOUT_VACANCIES_OVERVIEW);
				int fSalary = c
						.getColumnIndex(DBConstant.ABOUT_VACANCIES_SALARY);
				int fOpenedCompanyID = c
						.getColumnIndex(DBConstant.ABOUT_VACANCIES_OPENED_COMPANY_ID);
				int fLastUpdateDate = c
						.getColumnIndex(DBConstant.ABOUT_VACANCIES_LAST_UPDATE_DATE);

				String id = c.getString(idf);
				String title = c.getString(fTitle);
				String overview = c.getString(fOverview);
				String salary = c.getString(fSalary);
				String openedCompanyID = c.getString(fOpenedCompanyID);
				String lastUpdateDate = c.getString(fLastUpdateDate);

				info.put(DBConstant.ABOUT_VACANCIES_ID, id);
				info.put(DBConstant.ABOUT_VACANCIES_TITLE, title);
				info.put(DBConstant.ABOUT_VACANCIES_OVERVIEW, overview);
				info.put(DBConstant.ABOUT_VACANCIES_SALARY, salary);
				info.put(DBConstant.ABOUT_VACANCIES_LAST_UPDATE_DATE,
						lastUpdateDate);
				info.put(DBConstant.ABOUT_VACANCIES_OPENED_COMPANY_ID,
						openedCompanyID);
			} while (c.moveToNext());
		} else {
			// No data
		}
		db.close();
		return info;
	}

	public static void saveNewVacancies(Context context,
			ArrayList<HashMap<String, String>> vacancies) {
		DAO dao = new DAO(context);
		SQLiteDatabase db = dao.getWritableDatabase();
		deleteAllData(context, DBConstant.DB_NEW_VCANCIES_TABLE_NAME);
		try {
			for (HashMap<String, String> vacancy : vacancies) {
				ContentValues data = new ContentValues();

				data.put(DBConstant.ABOUT_VACANCIES_ID,
						vacancy.get(DBConstant.ABOUT_VACANCIES_ID));
				data.put(DBConstant.ABOUT_VACANCIES_OVERVIEW,
						vacancy.get(DBConstant.ABOUT_VACANCIES_OVERVIEW));
				data.put(DBConstant.ABOUT_VACANCIES_SALARY,
						vacancy.get(DBConstant.ABOUT_VACANCIES_SALARY));
				data.put(DBConstant.ABOUT_VACANCIES_TITLE,
						vacancy.get(DBConstant.ABOUT_VACANCIES_TITLE));
				data.put(DBConstant.ABOUT_VACANCIES_OPENED_COMPANY_ID, vacancy
						.get(DBConstant.ABOUT_VACANCIES_OPENED_COMPANY_ID));

				db.insertOrThrow(DBConstant.DB_NEW_VCANCIES_TABLE_NAME, null,
						data);
			}
		} catch (Exception e) {
			Log.e(LOG_TAG, "Ошибка!");
			e.printStackTrace();
		}
		db.close();
	}

	public static ArrayList<HashMap<String, String>> getNewVacancies(Context context) {
		ArrayList<HashMap<String, String>> newVacancies = new ArrayList<HashMap<String, String>>();
		DAO dao = new DAO(context);
		SQLiteDatabase db = dao.getReadableDatabase();
		Cursor c = db.query(DBConstant.DB_NEW_VCANCIES_TABLE_NAME, null,
				null, null, null, null, null);
		if (c.moveToFirst()) {
			do {
				int idf = c.getColumnIndex(DBConstant.ABOUT_VACANCIES_ID);
				int fTitle = c.getColumnIndex(DBConstant.ABOUT_VACANCIES_TITLE);
				int fOverview = c
						.getColumnIndex(DBConstant.ABOUT_VACANCIES_OVERVIEW);
				int fSalary = c
						.getColumnIndex(DBConstant.ABOUT_VACANCIES_SALARY);
				int fOpenedCompanyID = c
						.getColumnIndex(DBConstant.ABOUT_VACANCIES_OPENED_COMPANY_ID);

				String id = c.getString(idf);
				String title = c.getString(fTitle);
				String overview = c.getString(fOverview);
				String salary = c.getString(fSalary);
				String openedCompanyID = c.getString(fOpenedCompanyID);

				HashMap<String, String> info = new HashMap<String, String>();
				info .put(DBConstant.ABOUT_VACANCIES_ID, id);
				info.put(DBConstant.ABOUT_VACANCIES_TITLE, title);
				info.put(DBConstant.ABOUT_VACANCIES_OVERVIEW, overview);
				info.put(DBConstant.ABOUT_VACANCIES_SALARY, salary);
				info.put(DBConstant.ABOUT_VACANCIES_OPENED_COMPANY_ID,
						openedCompanyID);
				newVacancies.add(info);
			} while (c.moveToNext());
		} else { 
			
		}
		db.close();
		return newVacancies;
	}
}
