package com.dubkovapps.devby.model.dboperation;

/**
 * @author Николай Дубков
 * 
 */
public class DBConstant {
	public static final String DB_NAME = "DEV_BY";

	public static final String DB_NEWS_TABLE_NAME = "NEWS";
	public static final String DB_COMPANIES_TABLE_NAME = "COMPANIES";
	public static final String DB_ABOUT_COMPANIES_TABLE_NAME = "ABOUT_COMPANIES";
	public static final String DB_ABOUT_VACANCIES_TABLE_NAME = "ABOUT_VACANCIES";
	public static final String DB_NEW_VCANCIES_TABLE_NAME = "NEW_VACANCIES_TABLE_NAME";
	
	public static final String NEWS_ID = "id";
	public static final String NEWS_TITLE = "title";
	public static final String NEWS_PUB_DATE = "pubdate";
	public static final String NEWS_LINK = "guid";
	public static final String NEWS_PICTURE = "picture";
	public static final String NEWS_NEW_TEXT = "new_text";

	public static final String COMPANIES_ID = "id";
	public static final String COMPANIES_TITLE = "title";
	public static final String COMPANIES_REALNAME = "realname";
	public static final String COMPANIES_SUBDOMAIN = "subdomain";

	public static final String ABOUT_COMPANIES_ID = "id";
	public static final String ABOUT_COMPANIES_TITLE = "title";
	public static final String ABOUT_COMPANIES_REALNAME = "realname";
	public static final String ABOUT_COMPANIES_SUBDOMAIN = "subdomain";
	public static final String ABOUT_COMPANIES_SUMMARY_RATING = "summary_rating";
	public static final String ABOUT_COMPANIES_OVERVIEW = "overview";
	public static final String ABOUT_COMPANIES_LAST_UPDATE_DATE = "last_update_date";
	public static final String ABOUT_COMPANIES_VACANCIES_OPENED = "vacancies_opened";

	public static final String ABOUT_VACANCIES_ID = "id";
	public static final String ABOUT_VACANCIES_TITLE = "title";
	public static final String ABOUT_VACANCIES_OVERVIEW = "overview";
	public static final String ABOUT_VACANCIES_SALARY = "salary_value";
	public static final String ABOUT_VACANCIES_OPENED_COMPANY_ID = "opened_company_id";
	public static final String ABOUT_VACANCIES_LAST_UPDATE_DATE = "last_update_date";

	public static final String DB_VACANCIES_TABLE_NAME = "VACANCY";

	public static final String VACANCY_ID = "id";
	public static final String VACANCY_TITLE = "title";



}
