package com.dubkovapps.devby.model.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.dubkovapps.devby.R;
import com.dubkovapps.devby.model.dboperation.DBConstant;
import com.dubkovapps.devby.model.dboperation.DBOperations;

public class CompaniesAdapter extends BaseAdapter implements Filterable{

	private ArrayList<HashMap<String, String>> companiesList;
	private Context context;

	public CompaniesAdapter(Context context) {
		this.context = context;
		updateDataSet();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return companiesList.size();
	}

	@Override
	public HashMap<String, String> getItem(int id) {
		// TODO Auto-generated method stub
		return companiesList.get(id);
	}

	@Override
	public long getItemId(int id) {
		// TODO Auto-generated method stub
		return id;
	}

	public void updateDataSet() {
		companiesList = DBOperations.getAllCompanies(context);
	}

	@Override
	public View getView(int id, View convertView, ViewGroup parentView) {
		View view = convertView;
		if (view == null) {
			LayoutInflater inflater = LayoutInflater.from(context);
			view = inflater.inflate(R.layout.fragment_companies_item, null);
		}

		TextView title = (TextView) view
				.findViewById(R.id.fragment_companies_item_company_name);

		title.setText(companiesList.get(id).get(DBConstant.COMPANIES_TITLE));

		return view;
	}

	/**
	 * @return the companiesList
	 */
	public ArrayList<HashMap<String, String>> getCompaniesList() {
		return companiesList;
	}

	@Override
	public Filter getFilter() {

		return new Filter() {

			@Override
			protected void publishResults(CharSequence charSequence,
					FilterResults filterResults) {
				companiesList = (ArrayList<HashMap<String, String>>) filterResults.values;
				notifyDataSetChanged();
			}

			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				updateDataSet();
				List<HashMap<String, String>> filterResults = new ArrayList<HashMap<String, String>>();
				FilterResults results = new FilterResults();
				if (constraint != null && constraint.toString().length() > 0) {
					filterResults = FilterSearch(constraint, companiesList);
					results.values = filterResults;
					results.count = filterResults.size();
				} else {
					updateDataSet();
					results.values = companiesList;
					results.count = companiesList.size();
				}
				return results;
			}

			private List<HashMap<String, String>> FilterSearch(
					CharSequence charSequence,
					List<HashMap<String, String>> rawData) {
				ArrayList<HashMap<String, String>> result = new ArrayList<HashMap<String, String>>();
				for (HashMap<String, String> data : rawData) {

					if (String.valueOf(data.get(DBConstant.COMPANIES_TITLE))
							.toUpperCase()
							.contains(charSequence.toString().toUpperCase())) {
						result.add(data);
					}
				}
				return result;
			}
		};
	}

}
