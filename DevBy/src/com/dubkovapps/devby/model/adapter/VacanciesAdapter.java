package com.dubkovapps.devby.model.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.dubkovapps.devby.R;
import com.dubkovapps.devby.model.dboperation.DBConstant;
import com.dubkovapps.devby.model.dboperation.DBOperations;

public class VacanciesAdapter extends BaseAdapter implements Filterable {

	private ArrayList<HashMap<String, String>> vacancies;
	private Context context;

	public VacanciesAdapter(Context context) {
		this.context = context;
		this.vacancies = DBOperations.getAllVacancies(context);
	}

	public VacanciesAdapter(Context context,
			ArrayList<HashMap<String, String>> vacancies) {
		this.context = context;
		this.vacancies = vacancies;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return vacancies.size();
	}

	@Override
	public HashMap<String, String> getItem(int id) {
		// TODO Auto-generated method stub
		return vacancies.get(id);
	}

	@Override
	public long getItemId(int id) {
		// TODO Auto-generated method stub
		return id;
	}

	public void updateDataSet(ArrayList<HashMap<String, String>> vacancies) {
		this.vacancies = vacancies;
	}

	public void updateDataSet() {
		this.vacancies = DBOperations.getAllVacancies(context);
	}

	@Override
	public View getView(int id, View convertView, ViewGroup parentView) {
		View view = convertView;
		if (view == null) {
			LayoutInflater inflater = LayoutInflater.from(context);
			view = inflater.inflate(R.layout.vacancy_item, null);
		}

		TextView title = (TextView) view.findViewById(R.id.vacancy_item_title);

		title.setText(vacancies.get(id).get(DBConstant.VACANCY_TITLE));

		return view;
	}

	/**
	 * @return the companiesList
	 */
	public ArrayList<HashMap<String, String>> getCompaniesList() {
		return vacancies;
	}

	/*
	 * Фильтр. (non-Javadoc)
	 * 
	 * @see android.widget.Filterable#getFilter()
	 */
	@Override
	public Filter getFilter() {

		return new Filter() {

			@Override
			protected void publishResults(CharSequence charSequence,
					FilterResults filterResults) {
				vacancies = (ArrayList<HashMap<String, String>>) filterResults.values;
				notifyDataSetChanged();
			}

			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				updateDataSet();
				List<HashMap<String, String>> filterResults = new ArrayList<HashMap<String, String>>();
				FilterResults results = new FilterResults();
				if (constraint != null && constraint.toString().length() > 0) {
					filterResults = FilterSearch(constraint, vacancies);
					results.values = filterResults;
					results.count = filterResults.size();
				} else {
					updateDataSet();
					results.values = vacancies;
					results.count = vacancies.size();
				}
				return results;
			}

			private List<HashMap<String, String>> FilterSearch(
					CharSequence charSequence,
					List<HashMap<String, String>> rawData) {
				ArrayList<HashMap<String, String>> result = new ArrayList<HashMap<String, String>>();
				for (HashMap<String, String> data : rawData) {
				
					if (String.valueOf(data.get(DBConstant.ABOUT_VACANCIES_TITLE))
							.toUpperCase()
							.contains(charSequence.toString().toUpperCase())) {
						result.add(data);
					}
				}
				return result;
			}
		};
	}
}
