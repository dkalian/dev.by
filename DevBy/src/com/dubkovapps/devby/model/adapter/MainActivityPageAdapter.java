package com.dubkovapps.devby.model.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.dubkovapps.devby.controller.activities.fragments.CompaniesFragment;
import com.dubkovapps.devby.controller.activities.fragments.JobFragment;
import com.dubkovapps.devby.controller.activities.fragments.NewsFragment;

public class MainActivityPageAdapter extends FragmentPagerAdapter {

	public MainActivityPageAdapter(FragmentManager fm) {
		super(fm);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Fragment getItem(int position) {
		switch (position) {
		case 0:
			return JobFragment.newInstance(0);
		case 1:
			return NewsFragment.newInstance(1);
		case 2:
			return CompaniesFragment.newInstance(2);
		default:
			return null;
		}
	}

	@Override
	public CharSequence getPageTitle(int position) {
		switch (position) {
		case 0:
			return "Работа";

		case 1:

			return "Новости";

		case 2:

			return "Компании";

		default:
			break;
		}
		return super.getPageTitle(position);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 3;
	}

}
