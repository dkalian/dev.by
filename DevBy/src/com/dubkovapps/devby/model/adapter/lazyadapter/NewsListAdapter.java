package com.dubkovapps.devby.model.adapter.lazyadapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dubkovapps.devby.R;
import com.dubkovapps.devby.model.constant.NewsConstant;

;

/**
 * @author Nickolai
 * 
 */

public class NewsListAdapter extends BaseAdapter {

	/**
	 * Image Loader {@link ImageLoader}
	 */
	public ImageLoader imageLoader;

	private Context context;

	ArrayList<HashMap<String, String>> news = new ArrayList<HashMap<String, String>>();

	private List<String> newsTitles = new ArrayList<String>();
	private List<String> newsContents = new ArrayList<String>();
	private List<String> newsPubDates = new ArrayList<String>();
	private List<String> newsLinks = new ArrayList<String>();
	private List<String> newsPicturesLinks = new ArrayList<String>();

	/**
	 * @param context
	 *            activity context
	 * @param news
	 *            newsData
	 */

	public NewsListAdapter(Context context,
			ArrayList<HashMap<String, String>> news) {
		this.context = context;
		if (news != null)
			updateData(news);
	}

	public void updateDataSet(ArrayList<HashMap<String, String>> newData) {
		newsTitles.clear();
		newsContents.clear();
		newsPubDates.clear();
		newsLinks.clear();
		newsPicturesLinks.clear();
		news = newData;
		updateData(news);
	}

	private void updateData(ArrayList<HashMap<String, String>> data) {
		for (HashMap<String, String> hashMap : data) {
			newsTitles.add(hashMap.get(NewsConstant.NEWS_TITLE));
			newsContents.add(hashMap.get(NewsConstant.NEWS_NEW_TEXT));
			newsPubDates.add(hashMap.get(NewsConstant.NEWS_PUB_DATE));
			newsLinks.add(hashMap.get(NewsConstant.NEWS_LINK));
			newsPicturesLinks.add(hashMap.get(NewsConstant.NEWS_PICTURE));
			
			imageLoader = new ImageLoader(context);
		}
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return newsContents.size();
	}

	@Override
	public Object getItem(int pos) {
		// TODO Auto-generated method stub
		return news.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		// TODO Auto-generated method stub
		return pos;
	}
	
	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {

		View view = convertView;
		if (view == null) {
			LayoutInflater inflater = LayoutInflater.from(context);
			view = inflater.inflate(R.layout.fragment_news_item, null);
		}

		TextView title = (TextView) view
				.findViewById(R.id.fragment_news_item_title);
		TextView newText = (TextView) view
				.findViewById(R.id.fragment_news_item_new_text);
		TextView pubDate = (TextView) view
				.findViewById(R.id.fragment_news_item_pub_date);

		ImageView picture = (ImageView) view.findViewById(R.id.fragment_news_item_image);

		title.setText(newsTitles.get(pos));
		newText.setText(newsContents.get(pos));
		pubDate.setText(newsPubDates.get(pos));

		
		
		imageLoader.DisplayImage(newsPicturesLinks.get(pos), picture);

		return view;
	}

}
