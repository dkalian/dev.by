package com.dubkovapps.devby.model.sharedpreferences;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class AppSharedPreferences {

	public static String getStringPreferenceByKey(
			SharedPreferences sharedPreferences, String key) {
		if (sharedPreferences.contains(key)) {
			return sharedPreferences.getString(key, null);
		}
		return null;
	}

	public static void saveStringPreference(
			SharedPreferences sharedPreferences, String key, String value) {
		Editor editor = sharedPreferences.edit();
		editor.putString(key, value);
		editor.commit();
	}

	public static boolean getBooleanPreferenceByKey(
			SharedPreferences sharedPreferences, String key) {
		return sharedPreferences.getBoolean(key, true);
	}

	public static void saveBooleanPreference(
			SharedPreferences sharedPreferences, String key, boolean value) {
		Editor editor = sharedPreferences.edit();
		editor.putBoolean(key, value);
		editor.commit();
	}
}
