package com.dubkovapps.devby.controller.activities;

import java.util.HashMap;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.SpannableString;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.dubkovapps.devby.R;
import com.dubkovapps.devby.model.MyLeadingMarginSpan2;
import com.dubkovapps.devby.model.adapter.lazyadapter.ImageLoader;
import com.dubkovapps.devby.model.constant.Constant;
import com.dubkovapps.devby.model.constant.NewsConstant;

/**
 * @author Nickolai
 * 
 */

public class FullNewActivity extends ActionBarActivity { 
	
	TextView content, actionBarTitle;
	Button fullVersion;
	ImageView picture;

	HashMap<String, String> newData;

	ActionBar actionBar;
	
	static final String LOG_TAG = "Full New Activity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_full_new);

		initActionBar();
		initView();

		newData = getData();
		validateData(newData);
		fillData(newData);
	}

	void initActionBar() {
		actionBar = getSupportActionBar();
		actionBar
				.setCustomView(R.layout.custom_action_bar_view);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setBackgroundDrawable(getResources().getDrawable(R.color.main_color));
	}

	private void initView() {
		content = (TextView) findViewById(R.id.activity_full_new_content);
		View view = actionBar.getCustomView();
		actionBarTitle = (TextView) view.findViewById(R.id.custom_action_bar_view_title);
		actionBarTitle.setSelected(true);
		picture = (ImageView) findViewById(R.id.activity_full_new_picture);
		fullVersion = (Button) findViewById(R.id.activity_full_new_full_version);
	}

	void fillData(HashMap<String, String> data) {
		actionBarTitle.setText((CharSequence) data.get(NewsConstant.NEWS_TITLE));
		
		ImageLoader imageLoader = new ImageLoader(this);
		imageLoader.DisplayImage(newData.get(NewsConstant.NEWS_PICTURE),
				picture);
		int leftMargin = picture.getDrawable().getIntrinsicWidth()/2;
		int pictureHeigth = picture.getDrawable().getMinimumHeight();
		
		int lines = (int) (Math.round(pictureHeigth/3.5/16)-1);
		
		Log.d(LOG_TAG, "Left margin: "+leftMargin);
		Log.d(LOG_TAG, "pictureHeigth: "+pictureHeigth);
		
		SpannableString ss = new SpannableString((CharSequence) data.get(NewsConstant.NEWS_NEW_TEXT));
        // Выставляем отступ для первых восьми строк абазца
        ss.setSpan(new MyLeadingMarginSpan2(lines, leftMargin), 0, ss.length(), 0);

        content.setText(ss);
        
		Animation leftToRightTranslate = AnimationUtils.loadAnimation(this,
				R.anim.action_bar_title_anim); // Animation translate
		actionBarTitle.startAnimation(leftToRightTranslate);
	}

	private HashMap<String, String> getData() {
		HashMap<String, String> data = new HashMap<String, String>();
		Bundle extras = getIntent().getExtras();
		data = (HashMap<String, String>) extras
				.getSerializable(Constant.EXTRA_NEW_DATA);
		fullVersion.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri
						.parse(newData.get(NewsConstant.NEWS_LINK)));
				startActivity(intent);
			}
		});
		return data;
	}

	private void validateData(HashMap<String, String> data) {
		if (data == null) {

		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.full_new, menu);
		return true;
	}
}
