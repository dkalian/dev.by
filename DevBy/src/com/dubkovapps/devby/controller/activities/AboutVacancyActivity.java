package com.dubkovapps.devby.controller.activities;

import java.util.HashMap;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import com.dubkovapps.devby.R;
import com.dubkovapps.devby.model.constant.Constant;
import com.dubkovapps.devby.model.constant.NewsConstant;
import com.dubkovapps.devby.model.dboperation.DBConstant;
import com.dubkovapps.devby.model.dboperation.DBOperations;

public class AboutVacancyActivity extends ActionBarActivity {

	ActionBar actionBar;

	TextView overview, salary;

	Button onSite;

	String vacancyID;
	String openedCompanyID;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about_vacancy);

		overview = (TextView) findViewById(R.id.activity_about_vacancy_overview);
		salary = (TextView) findViewById(R.id.activity_about_vacancy_salary);
		onSite = (Button) findViewById(R.id.activity_about_vacancy_nutton_on_site);
		onSite.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri
						.parse("http://jobs.dev.by/" + vacancyID));
				startActivity(intent);
			}
		});

		HashMap<String, String> vacancy = (HashMap<String, String>) getIntent()
				.getExtras().get(Constant.EXTRA_VACANCY_DATA);

		vacancyID = vacancy.get(DBConstant.ABOUT_VACANCIES_ID);

		initActionBar(vacancy.get(DBConstant.ABOUT_VACANCIES_TITLE));

		vacancy = DBOperations.getVacacyInfo(this, vacancyID);

		System.out.println(vacancy);

		openedCompanyID = vacancy
				.get(DBConstant.ABOUT_VACANCIES_OPENED_COMPANY_ID);

		String salaryStr = vacancy.get(DBConstant.ABOUT_VACANCIES_SALARY);
		if (salaryStr.compareTo("null") == 0) {
			salary.setText("не указана");
		} else {
			salary.setText(salaryStr);
		}
		overview.setText(Html
				.fromHtml(vacancy.get(DBConstant.ABOUT_VACANCIES_OVERVIEW))
				.toString().trim());
	}

	void initActionBar(String title) {
		actionBar = getSupportActionBar();
		actionBar.setCustomView(R.layout.custom_action_bar_view);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setBackgroundDrawable(getResources().getDrawable(
				R.color.main_color));

		TextView actionBarTitle = (TextView) findViewById(R.id.custom_action_bar_view_title);
		actionBarTitle.setSelected(true);
		actionBarTitle.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
		actionBarTitle.setText(title);
		Animation leftToRightTranslate = AnimationUtils.loadAnimation(this,
				R.anim.action_bar_title_anim); // Animation translate
		actionBarTitle.startAnimation(leftToRightTranslate);
	}
}
