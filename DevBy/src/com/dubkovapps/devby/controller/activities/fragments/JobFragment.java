package com.dubkovapps.devby.controller.activities.fragments;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnCloseListener;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.text.Html;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.dubkovapps.devby.R;
import com.dubkovapps.devby.controller.activities.AboutVacancyActivity;
import com.dubkovapps.devby.model.HTTP.PrCyClient;
import com.dubkovapps.devby.model.adapter.VacanciesAdapter;
import com.dubkovapps.devby.model.connection.NetworkManager;
import com.dubkovapps.devby.model.constant.Constant;
import com.dubkovapps.devby.model.constant.SharedPreferencesConstant;
import com.dubkovapps.devby.model.dboperation.DBConstant;
import com.dubkovapps.devby.model.dboperation.DBOperations;
import com.dubkovapps.devby.model.sharedpreferences.AppSharedPreferences;
import com.dubkovapps.devby.model.view.CustomToast;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

public class JobFragment extends Fragment {

	static final String PULL_LABEL = "      Потяните для обновления";
	static final String RELEASE_LABEL = "      Отпустите для обновления";
	static final String LAST_UPDATE_DATE_NULL = "Не обновлялось";
	private static final String CONNECTION_FAILED_MESSAGE = "Нет соединения с Интернетом. Проверьте ваше соединение и попытайтесь снова.";

	public static JobFragment newInstance(int page) {
		JobFragment jobFragment = new JobFragment();
		Bundle arguments = new Bundle();
		jobFragment.setArguments(arguments);
		return jobFragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		vacanciesAdapter = new VacanciesAdapter(getActivity());
	}

	RelativeLayout helpMessage;
	VacanciesAdapter vacanciesAdapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_job, null);
		helpMessage = (RelativeLayout) view
				.findViewById(R.id.fragment_job_help_message);
		updateHelpMessage();
		initListNews(view);
		setHasOptionsMenu(true);
		return view;
	}

	private void checkAutoFilter() {
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(getActivity());
		if (sp.getBoolean("auto_filter", false)
				&& sp.getString("vacancy_name", "").length() != 0) {
			vacanciesAdapter.getFilter().filter(
					sp.getString("vacancy_name", ""));
		}
	}

	@Override
	public void onResume() {
		checkAutoFilter();
		super.onResume();
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.job_fragent_menu, menu);
		MenuItem searchItem = menu.findItem(R.id.job_fragment_menu_search);
		SearchView searchView = (SearchView) MenuItemCompat
				.getActionView(searchItem);
		searchView.setOnQueryTextListener(onQueryTextListener);

		searchView.setQueryHint(Html.fromHtml("<font color = #ffffff>"
				+ "Поиск" + "</font>"));
		searchView.setOnCloseListener(new OnCloseListener() {
			@Override
			public boolean onClose() {
				checkAutoFilter();
				return false;
			}
		});

	}

	OnQueryTextListener onQueryTextListener = new OnQueryTextListener() {

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.support.v7.widget.SearchView.OnQueryTextListener#
		 * onQueryTextSubmit(java.lang.String)
		 */
		@Override
		public boolean onQueryTextSubmit(String string) {
			// TODO Auto-generated method stub
			return false;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.support.v7.widget.SearchView.OnQueryTextListener#
		 * onQueryTextChange(java.lang.String)
		 */
		@Override
		public boolean onQueryTextChange(String string) {
			vacanciesAdapter.getFilter().filter(string);
			return false;
		}
	};

	PullToRefreshListView vacanciesListView;

	@SuppressWarnings("deprecation")
	protected void initListNews(View view) {
		System.out.println("OnCreateView");
		vacanciesListView = (PullToRefreshListView) view
				.findViewById(R.id.fragment_job_vacancies_list);
		vacanciesListView.setReleaseLabel(RELEASE_LABEL);
		vacanciesListView.setPullLabel(PULL_LABEL);

		vacanciesListView.setAdapter(vacanciesAdapter);

		String lastUpdatedDate = AppSharedPreferences.getStringPreferenceByKey(
				getActivity().getSharedPreferences(
						SharedPreferencesConstant.PREFERENCES_NAME,
						Context.MODE_PRIVATE),
				SharedPreferencesConstant.PREFERENCE_LAST_JOB_UPDATE);

		if (lastUpdatedDate != null) {
			vacanciesListView.setLastUpdatedLabel(lastUpdatedDate);
		} else {
			vacanciesListView.setLastUpdatedLabel(LAST_UPDATE_DATE_NULL);
		}

		vacanciesListView
				.setOnRefreshListener(new OnRefreshListener<ListView>() {
					@Override
					public void onRefresh(
							PullToRefreshBase<ListView> refreshView) {
						if (NetworkManager
								.isNetworkConnectionAvailable(getActivity())) {
							new JobParserThread().execute();
						} else {
							CustomToast.getErrorToast(getActivity(),
									CONNECTION_FAILED_MESSAGE,
									Toast.LENGTH_SHORT).show();
						}
					}
				});

		vacanciesListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int pos,
					long id) {
				Intent intent = new Intent(getActivity(),
						AboutVacancyActivity.class);
				intent.putExtra(Constant.EXTRA_VACANCY_DATA,
						vacanciesAdapter.getItem(pos - 1));
				startActivity(intent);
			}
		});
	}

	public class JobParserThread extends AsyncTask<Void, Void, String> {
		String lastUpdated = LAST_UPDATE_DATE_NULL;

		final String LOG_TAG = "Job parser thread";

		boolean error = false;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		InputStream inputStream = null;
		private HttpClient client;
		private HttpGet httpGet;
		private BufferedReader in;

		@Override
		protected String doInBackground(Void... params) {
			client = PrCyClient.getInstance().client;
			httpGet = new HttpGet(Constant.API_DEV_BY_ALL_JOBS_URL);
			try {
				HttpResponse response = client.execute(httpGet);
				in = new BufferedReader(new InputStreamReader(response
						.getEntity().getContent()));
				StringBuffer sb = new StringBuffer("");
				String line = "";
				while ((line = in.readLine()) != null) {
					sb.append(line + "\n");
				}
				in.close();
				String result = sb.toString();
				JSONArray jobs = new JSONArray(result);

				String lastUpdated = DateUtils.formatDateTime(getActivity(),
						System.currentTimeMillis(), DateUtils.FORMAT_SHOW_TIME
								| DateUtils.FORMAT_SHOW_DATE
								| DateUtils.FORMAT_ABBREV_ALL);
				ArrayList<HashMap<String, String>> vacancies = new ArrayList<HashMap<String, String>>();

				for (int i = 0; i < jobs.length(); i++) {
					JSONObject job = jobs.getJSONObject(i).getJSONObject("job");
					HashMap<String, String> vacancy = new HashMap<String, String>();
					vacancy.put(DBConstant.ABOUT_VACANCIES_ID,
							job.getString(DBConstant.ABOUT_VACANCIES_ID));
					vacancy.put(DBConstant.ABOUT_VACANCIES_TITLE,
							job.getString(DBConstant.ABOUT_VACANCIES_TITLE));
					vacancy.put(DBConstant.ABOUT_VACANCIES_OVERVIEW,
							job.getString(DBConstant.ABOUT_VACANCIES_OVERVIEW));
					vacancy.put(DBConstant.ABOUT_VACANCIES_SALARY,
							job.getString(DBConstant.ABOUT_VACANCIES_SALARY));
					vacancy.put(DBConstant.ABOUT_VACANCIES_LAST_UPDATE_DATE,
							lastUpdated);
					vacancy.put(DBConstant.ABOUT_VACANCIES_OPENED_COMPANY_ID,
							job.getString("company_id"));
					vacancies.add(vacancy);
				}
				DBOperations.deleteAllData(getActivity(),
						DBConstant.DB_ABOUT_VACANCIES_TABLE_NAME);
				DBOperations.saveInfoAboutVacancies(getActivity(), vacancies);
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (IllegalStateException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			vacanciesListView.onRefreshComplete();
			lastUpdated = DateUtils.formatDateTime(getActivity(),
					System.currentTimeMillis(), DateUtils.FORMAT_SHOW_TIME
							| DateUtils.FORMAT_SHOW_DATE
							| DateUtils.FORMAT_ABBREV_ALL);

			vacanciesListView.setLastUpdatedLabel(lastUpdated);
			// сохраняем дату последнего обновления в настройки
			AppSharedPreferences.saveStringPreference(
					getActivity().getSharedPreferences(
							SharedPreferencesConstant.PREFERENCES_NAME,
							Context.MODE_PRIVATE),
					SharedPreferencesConstant.PREFERENCE_LAST_JOB_UPDATE,
					lastUpdated);

			helpMessage.setVisibility(View.INVISIBLE);
			vacanciesAdapter.updateDataSet();
			vacanciesAdapter.notifyDataSetChanged();
			checkAutoFilter();
			updateHelpMessage();
		}
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		if (vacanciesAdapter != null) {
			vacanciesAdapter.updateDataSet();
			vacanciesAdapter.notifyDataSetChanged();
			updateHelpMessage();
			checkAutoFilter();
		}
	};

	void updateHelpMessage() {
		if (vacanciesAdapter.getCount() == 0) {
			helpMessage.setVisibility(View.VISIBLE);
		} else {
			helpMessage.setVisibility(View.GONE);
		}
	}

}
