package com.dubkovapps.devby.controller.activities.fragments;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.dubkovapps.devby.R;
import com.dubkovapps.devby.controller.activities.FullNewActivity;
import com.dubkovapps.devby.controller.activities.MainActivity;
import com.dubkovapps.devby.model.HTTP.PrCyClient;
import com.dubkovapps.devby.model.adapter.lazyadapter.NewsListAdapter;
import com.dubkovapps.devby.model.constant.Constant;
import com.dubkovapps.devby.model.constant.NewsConstant;
import com.dubkovapps.devby.model.constant.SharedPreferencesConstant;
import com.dubkovapps.devby.model.dboperation.DBConstant;
import com.dubkovapps.devby.model.dboperation.DBOperations;
import com.dubkovapps.devby.model.sharedpreferences.AppSharedPreferences;
import com.dubkovapps.devby.model.view.CustomToast;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

/**
 * @author Nickolai
 * 
 */

public class NewsFragment extends Fragment {

	static final String INTERENT_ACCESS_ERROR = "Ошибка при подключении к Интернету.";
	static final String CACHE_DATA_ERROR = "Ошибка при кэшировании данных.";
	static final String PULL_LABEL = "      Потяните для обновления";
	static final String RELEASE_LABEL = "      Отпустите для обновления";
	static final String LAST_UPDATE_DATE_NULL = "Не обновлялось";

	final String LOG_TAG = "News Fragment";

	private static boolean isNewsThreadStarted = false;
	NewsParserThread newsParserThread;

	PullToRefreshListView newsList; // Список новостей
	NewsListAdapter newsListAdapter; // Адаптер с новостями
	ArrayList<HashMap<String, String>> news = null; // Данные

	RelativeLayout helpMessage;

	public static NewsFragment newInstance(int page) {
		NewsFragment newsFragment = new NewsFragment();
		Bundle arguments = new Bundle();
		newsFragment.setArguments(arguments);
		return newsFragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// setHasOptionsMenu(true);
		super.onCreate(savedInstanceState);
		System.out.println("IS Thread started: " + isNewsThreadStarted);
		if (newsParserThread == null) {
			newsParserThread = createNewsParser();
		}
		newsParserThread.link((MainActivity) getActivity());
		setHasOptionsMenu(true);
	}

	private NewsParserThread createNewsParser() {
		NewsParserThread parserThread = new NewsParserThread();
		parserThread.link((MainActivity) getActivity());
		return parserThread;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_news, null);
		news = DBOperations.getAllRecords(getActivity());
		helpMessage = (RelativeLayout) view
				.findViewById(R.id.fragment_news_help_message);
		initListNews(view);
		if (isEmptyCachedData()) {
			helpMessage.setVisibility(View.VISIBLE);
		} else {
			helpMessage.setVisibility(View.INVISIBLE);
		}
		return view;
	}

	boolean isEmptyCachedData() {
		if ((news.size() == 0) || (news == null)) {
			return true;
		}
		return false;
	}

	protected void initListNews(View view) {
		System.out.println("OnCreateView");
		newsList = (PullToRefreshListView) view
				.findViewById(R.id.fragment_news_news_list);
		newsList.setReleaseLabel(RELEASE_LABEL);
		newsList.setPullLabel(PULL_LABEL);

		String lastUpdatedDate = AppSharedPreferences.getStringPreferenceByKey(
				getActivity().getSharedPreferences(
						SharedPreferencesConstant.PREFERENCES_NAME,
						Context.MODE_PRIVATE),
				SharedPreferencesConstant.PREFERENCE_LAST_NEWS_UPDATE);
		if (lastUpdatedDate != null) {
			newsList.setLastUpdatedLabel(lastUpdatedDate);
		} else {
			newsList.setLastUpdatedLabel(LAST_UPDATE_DATE_NULL);
		}

		newsList.setOnRefreshListener(new OnRefreshListener<ListView>() {
			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				Log.e(LOG_TAG, "OnRefreshing");
				if (!isNewsThreadStarted) {
					Log.e(LOG_TAG, "Not refreshing! Starting thread");
					newsParserThread.execute();
				} else {
					Log.e(LOG_TAG, "Already started!");
				}
			}
		});

		newsListAdapter = new NewsListAdapter(getActivity(), news);
		newsList.setAdapter(newsListAdapter);
		newsList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Intent fullNewIntent = new Intent(getActivity(),
						FullNewActivity.class);
				fullNewIntent.putExtra(Constant.EXTRA_NEW_DATA,
						news.get((int) id));
				startActivity(fullNewIntent);
			}
		});
	}

	public Object onRetainNonConfigurationInstance() {
		// удаляем из MyTask ссылку на старое MainActivity
		newsParserThread.unLink();
		return newsParserThread;
	}

	public class NewsParserThread extends AsyncTask<Void, Void, String> {
		String lastUpdated = LAST_UPDATE_DATE_NULL;

		final String LOG_TAG = "News Parser Thread";

		boolean error = false;

		MainActivity activity;

		// получаем ссылку на MainActivity
		void link(MainActivity act) {
			activity = act;
		}

		// обнуляем ссылку
		void unLink() {
			activity = null;
		}

		@Override
		protected void onPreExecute() {
			isNewsThreadStarted = true;
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(Void... params) {
			news = new ArrayList<HashMap<String, String>>();
			Log.e(LOG_TAG, "THREAD STARTED!!");
			Document doc;
			try {
				doc = Jsoup.connect(Constant.DEV_BY_RSS_URL).get();
				Elements itemElements = doc.getElementsByTag("item");
				for (org.jsoup.nodes.Element element : itemElements) {
					HashMap<String, String> temp = new HashMap<String, String>();
					String title = element.select(NewsConstant.NEWS_TITLE)
							.text();

					String description = element.select(
							NewsConstant.NEWS_DESCRIPTION).text();
					// Парсируем нод description, вынимаем тег image и забираем
					// аттрибут scr
					Document descriptionDoc = Jsoup.parse(description);
					String pictureURL = descriptionDoc.select("img").attr("src")
									.toString();

					// вынимаем тег p (содержит текст новости)
					String newText = descriptionDoc.select("p").text();

					String link = element.select(NewsConstant.NEWS_LINK).text();
					String pubDate = element.select(NewsConstant.NEWS_PUB_DATE)
							.text();

					temp.put(NewsConstant.NEWS_TITLE, title);
					temp.put(NewsConstant.NEWS_NEW_TEXT, newText);
					temp.put(NewsConstant.NEWS_PUB_DATE, pubDate);
					temp.put(NewsConstant.NEWS_LINK, link);
					temp.put(NewsConstant.NEWS_PICTURE, pictureURL);

					news.add(temp);
					DBOperations.deleteAllData(activity,
							DBConstant.DB_NEWS_TABLE_NAME);
					DBOperations.addRecords(activity, news);
				}
			} catch (IOException e) {
				e.printStackTrace();
				error = true;
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			Log.i(LOG_TAG, "Thread finish work");
			if (!error) {
				newsList.onRefreshComplete();
				newsListAdapter.updateDataSet(news);
				newsListAdapter.notifyDataSetChanged();
				// Приводим дату в нужный формат
				lastUpdated = DateUtils.formatDateTime(activity,
						System.currentTimeMillis(), DateUtils.FORMAT_SHOW_TIME
								| DateUtils.FORMAT_SHOW_DATE
								| DateUtils.FORMAT_ABBREV_ALL);

				// устанавливаем новую дату последнего обновления
				newsList.setLastUpdatedLabel(lastUpdated);

				// сохраняем дату последнего обновления в настройки
				AppSharedPreferences.saveStringPreference(activity
						.getSharedPreferences(
								SharedPreferencesConstant.PREFERENCES_NAME,
								Context.MODE_PRIVATE),
						SharedPreferencesConstant.PREFERENCE_LAST_NEWS_UPDATE,
						lastUpdated);

				newsList.onRefreshComplete();
				isNewsThreadStarted = false;
				newsParserThread = createNewsParser();
				Log.i(LOG_TAG, "No errors");
				helpMessage.setVisibility(View.INVISIBLE);
			} else {
				newsList.onRefreshComplete();
				CustomToast.getErrorToast(activity, INTERENT_ACCESS_ERROR,
						Toast.LENGTH_LONG).show();
				isNewsThreadStarted = false;
				Log.e(LOG_TAG, "Errors!");
				newsParserThread = createNewsParser();
			}
			Log.i(LOG_TAG, "Is Thread Started: " + isNewsThreadStarted);
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.news_fragment_menu, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

}
