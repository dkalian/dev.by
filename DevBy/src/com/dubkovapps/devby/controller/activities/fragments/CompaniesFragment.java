package com.dubkovapps.devby.controller.activities.fragments;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnCloseListener;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.text.Html;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.dubkovapps.devby.R;
import com.dubkovapps.devby.controller.activities.AboutCompanyActivity;
import com.dubkovapps.devby.model.HTTP.PrCyClient;
import com.dubkovapps.devby.model.adapter.CompaniesAdapter;
import com.dubkovapps.devby.model.connection.NetworkManager;
import com.dubkovapps.devby.model.constant.Constant;
import com.dubkovapps.devby.model.constant.SharedPreferencesConstant;
import com.dubkovapps.devby.model.dboperation.DBConstant;
import com.dubkovapps.devby.model.dboperation.DBOperations;
import com.dubkovapps.devby.model.sharedpreferences.AppSharedPreferences;
import com.dubkovapps.devby.model.view.CustomToast;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

/**
 * @author Николай Дубков
 * 
 */
public class CompaniesFragment extends Fragment {

	private static final String CONNECTION_FAILED_MESSAGE = "Нет соединения с Интернетом. Проверьте ваше соединение и попытайтесь снова.";

	public static Fragment newInstance(int page) {
		CompaniesFragment companiesFragment = new CompaniesFragment();
		return companiesFragment;
	}

	CompaniesAdapter companiesAdapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		companiesAdapter = new CompaniesAdapter(getActivity());
		super.onCreate(savedInstanceState);
	}

	RelativeLayout helpMessage;
	private PullToRefreshListView companiesList;
	static final String PULL_LABEL = "      Потяните для обновления";
	static final String RELEASE_LABEL = "      Отпустите для обновления";
	static final String LAST_UPDATE_DATE_NULL = "Не обновлялось";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_companies, null);
		// Подсказка на экране
		helpMessage = (RelativeLayout) view
				.findViewById(R.id.fragment_companies_help_message);
		updateHelpMessage();
		initListNews(view);
		setHasOptionsMenu(true);
		return view;

	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.companies_fragment_menu, menu);
		MenuItem searchItem = menu
				.findItem(R.id.companies_fragment_menu_search);
		SearchView searchView = (SearchView) MenuItemCompat
				.getActionView(searchItem);
		searchView.setQueryHint("Поиск");
		searchView.setOnQueryTextListener(onQueryTextListener);

		searchView.setQueryHint(Html.fromHtml("<font color = #ffffff>" + "Поиск" + "</font>"));
		searchView.setOnCloseListener(new OnCloseListener() {
			@Override
			public boolean onClose() {
				return false;
			}
		});

	}

	OnQueryTextListener onQueryTextListener = new OnQueryTextListener() {

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.support.v7.widget.SearchView.OnQueryTextListener#
		 * onQueryTextSubmit(java.lang.String)
		 */
		@Override
		public boolean onQueryTextSubmit(String string) {
			// TODO Auto-generated method stub
			return false;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.support.v7.widget.SearchView.OnQueryTextListener#
		 * onQueryTextChange(java.lang.String)
		 */
		@Override
		public boolean onQueryTextChange(String string) {
			companiesAdapter.getFilter().filter(string);
			return false;
		}
	};

	@SuppressWarnings("deprecation")
	protected void initListNews(View view) {
		System.out.println("OnCreateView");
		companiesList = (PullToRefreshListView) view
				.findViewById(R.id.fragment_companies_companies_list);
		companiesList.setReleaseLabel(RELEASE_LABEL);
		companiesList.setPullLabel(PULL_LABEL);

		companiesList.setAdapter(companiesAdapter);

		String lastUpdatedDate = AppSharedPreferences.getStringPreferenceByKey(
				getActivity().getSharedPreferences(
						SharedPreferencesConstant.PREFERENCES_NAME,
						Context.MODE_PRIVATE),
				SharedPreferencesConstant.PREFERENCE_LAST_COMPANIES_UPDATE);
		if (lastUpdatedDate != null) {
			companiesList.setLastUpdatedLabel(lastUpdatedDate);
		} else {
			companiesList.setLastUpdatedLabel(LAST_UPDATE_DATE_NULL);
		}

		companiesList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int pos,
					long id) {
				Intent intent = new Intent(getActivity(),
						AboutCompanyActivity.class);
				intent.putExtra(Constant.EXTRA_COMPANY_ID, companiesAdapter
						.getItem(pos - 1).get(DBConstant.COMPANIES_ID));
				intent.putExtra(Constant.EXTRA_COMPANY_TITLE, companiesAdapter
						.getItem(pos - 1).get(DBConstant.COMPANIES_TITLE));
				startActivity(intent);
			}
		});

		companiesList.setOnRefreshListener(new OnRefreshListener<ListView>() {
			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				if (NetworkManager.isNetworkConnectionAvailable(getActivity())) {
					new CompaniesParserThread().execute();
				} else {
					CustomToast.getErrorToast(getActivity(),
							CONNECTION_FAILED_MESSAGE, Toast.LENGTH_SHORT)
							.show();
					companiesList.onRefreshComplete();
				}
			}
		});
	}

	public class CompaniesParserThread extends AsyncTask<Void, Void, String> {
		String lastUpdated = LAST_UPDATE_DATE_NULL;

		final String LOG_TAG = "News Parser Thread";

		boolean error = false;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		InputStream inputStream = null;
		private HttpClient client;
		private HttpGet httpGet;
		private BufferedReader in;

		@Override
		protected String doInBackground(Void... params) {
			client = PrCyClient.getInstance().client;
			httpGet = new HttpGet(Constant.API_DEV_BY_ALL_COMPANIES_URL);
			try {
				HttpResponse response = client.execute(httpGet);
				in = new BufferedReader(new InputStreamReader(response
						.getEntity().getContent()));
				StringBuffer sb = new StringBuffer("");
				String line = "";
				while ((line = in.readLine()) != null) {
					sb.append(line + "\n");
				}
				in.close();
				String result = sb.toString();
				JSONArray companiesArray = new JSONArray(result);
				DBOperations.deleteAllData(getActivity(),
						DBConstant.DB_COMPANIES_TABLE_NAME);
				for (int i = 0; i < companiesArray.length(); i++) {
					JSONObject company = companiesArray.getJSONObject(i)
							.getJSONObject("company");
					ContentValues cv = new ContentValues();
					cv.put(DBConstant.COMPANIES_ID, company.getString("id"));
					cv.put(DBConstant.COMPANIES_TITLE,
							company.getString("title"));
					cv.put(DBConstant.COMPANIES_REALNAME,
							company.getString("realname"));
					cv.put(DBConstant.COMPANIES_SUBDOMAIN,
							company.getString("subdomain"));
					DBOperations.addCompany(getActivity(), cv);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (Exception e) {
				// TODO: handle exception
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			companiesList.onRefreshComplete();
			try { 
			lastUpdated = DateUtils.formatDateTime(getActivity(),
					System.currentTimeMillis(), DateUtils.FORMAT_SHOW_TIME
							| DateUtils.FORMAT_SHOW_DATE
							| DateUtils.FORMAT_ABBREV_ALL);

			companiesList.setLastUpdatedLabel(lastUpdated);

			// сохраняем дату последнего обновления в настройки

			AppSharedPreferences.saveStringPreference(
					getActivity().getSharedPreferences(
							SharedPreferencesConstant.PREFERENCES_NAME,
							Context.MODE_PRIVATE),
					SharedPreferencesConstant.PREFERENCE_LAST_COMPANIES_UPDATE,
					lastUpdated);

			helpMessage.setVisibility(View.INVISIBLE);
			companiesAdapter.updateDataSet();
			companiesAdapter.notifyDataSetChanged();
			updateHelpMessage();
			} catch (Exception e) { 
				e.printStackTrace();
			}
		}
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		if (companiesAdapter != null) {
			companiesAdapter.updateDataSet();
			companiesAdapter.notifyDataSetChanged();
			updateHelpMessage();
		}
	};

	void updateHelpMessage() {
		if (companiesAdapter.getCount() == 0) {
			helpMessage.setVisibility(View.VISIBLE);
		} else {
			helpMessage.setVisibility(View.GONE);
		}
	}
}
