package com.dubkovapps.devby.controller.activities;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.dubkovapps.devby.R;
import com.dubkovapps.devby.model.adapter.VacanciesAdapter;
import com.dubkovapps.devby.model.constant.Constant;

public class VacanciesListActivity extends Activity {

	ListView vacanciesListView;
	
	VacanciesAdapter adapter;

	private ArrayList<HashMap<String, String>> vacancies;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_vacancies_list);
		getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.WHITE ));
		
		
		vacanciesListView = (ListView) findViewById(R.id.activity_vacancies_vacancies_list);
		
		vacancies = (ArrayList<HashMap<String, String>>) getIntent().getExtras().get(Constant.EXTRA_COMPANY_VACANCIES_LIST);
		System.out.println(vacancies);
		adapter = new VacanciesAdapter(this, vacancies);
		vacanciesListView.setAdapter(adapter);
		
		vacanciesListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int pos,
					long id) {
				System.out.println(adapter.getItem(pos));
				startActivity(new Intent(VacanciesListActivity.this ,AboutVacancyActivity.class).putExtra(Constant.EXTRA_VACANCY_DATA, adapter.getItem(pos)));
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.vacancies_list, menu);
		return true;
	}

}
