package com.dubkovapps.devby.controller.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.dubkovapps.devby.PreferenceActivity;
import com.dubkovapps.devby.R;
import com.dubkovapps.devby.model.adapter.MainActivityPageAdapter;
import com.dubkovapps.devby.model.service.Autosearch;

/**
 * @author Николай Дубков
 * 
 */
public class MainActivity extends ActionBarActivity {

	final String LOG_TAG = "Main Activity";

	final String ACTIVITY_TITLE = "Все о работе в IT";

	ViewPager pager;
	ActionBar actionBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(
				"auto_search", false)) {
			startService(new Intent(this, Autosearch.class));
		}

		actionBar = initActionBar();
		initViewPager();

		View actionBarView = actionBar.getCustomView();
		TextView title = (TextView) actionBarView
				.findViewById(R.id.custom_action_bar_view_title);
		title.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
		title.setText(ACTIVITY_TITLE);
		Animation translate = AnimationUtils.loadAnimation(this,
				R.anim.action_bar_title_anim);
		title.startAnimation(translate);
	}

	void initViewPager() {
		pager = (ViewPager) findViewById(R.id.pager);
		pager.setAdapter(new MainActivityPageAdapter(
				getSupportFragmentManager()));

		PagerTabStrip pagerTabStrip = (PagerTabStrip) findViewById(R.id.pagerTabStrip);
		pagerTabStrip.setDrawFullUnderline(true);
		pagerTabStrip.setTextColor(Color.WHITE);
		pagerTabStrip.setTabIndicatorColor(Color.WHITE);

		pager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				Log.d("DEV.BY", "onPageSelected, position = " + position);
			}

			@Override
			public void onPageScrolled(int position, float positionOffset,
					int positionOffsetPixels) {

			}

			@Override
			public void onPageScrollStateChanged(int state) {
			}

		});
		pager.setCurrentItem(1);
	}

	ActionBar initActionBar() {
		ActionBar actionBar = getSupportActionBar();
		actionBar.setCustomView(R.layout.custom_action_bar_view);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setBackgroundDrawable(getResources().getDrawable(
				R.color.main_color));
		return actionBar;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main_activity_menu, menu);
		MenuItem settings = menu.findItem(R.id.action_settings);
		settings.setOnMenuItemClickListener(new OnMenuItemClickListener() {

			@Override
			public boolean onMenuItemClick(MenuItem item) {
				startActivity(new Intent(MainActivity.this,
						PreferenceActivity.class));
				return false;
			}
		});
		return true;
	}
}
