package com.dubkovapps.devby.controller.activities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.text.format.DateUtils;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dubkovapps.devby.R;
import com.dubkovapps.devby.model.HTTP.PrCyClient;
import com.dubkovapps.devby.model.connection.NetworkManager;
import com.dubkovapps.devby.model.constant.Constant;
import com.dubkovapps.devby.model.constant.NewsConstant;
import com.dubkovapps.devby.model.dboperation.DBConstant;
import com.dubkovapps.devby.model.dboperation.DBOperations;
import com.dubkovapps.devby.model.view.CustomToast;

/**
 * @author Николай Дубков
 * 
 */
public class AboutCompanyActivity extends ActionBarActivity {

	String companyID;
	RelativeLayout loadingMessage;
	RelativeLayout content;
	TextView lastUpdateDate;
	TextView overview, rating;
	TextView numberOfOpenedVacancies;

	Button vacanciesButton, onPageButton;

	ActionBar actionBar;

	private static final String CONNECTION_FAILED_MESSAGE = "Нет соединения с Интернетом. Проверьте ваше соединение и попытайтесь снова.";
	private static final String LAST_UPDATE_MESSAGE = "Локальная копия. Последнее обновление данных: ";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about_company);

		overview = (TextView) findViewById(R.id.activity_about_company_overwiew);
		rating = (TextView) findViewById(R.id.activity_about_company_rating);
		lastUpdateDate = (TextView) findViewById(R.id.activity_about_company_last_update_date);
		lastUpdateDate.setVisibility(View.GONE);
		numberOfOpenedVacancies = (TextView) findViewById(R.id.activity_about_company_number_of_opened_vacancies);

		vacanciesButton = (Button) findViewById(R.id.activity_about_company_vacancies_button);
		onPageButton = (Button) findViewById(R.id.activity_about_company_on_page_button);
		
		vacanciesButton.setOnClickListener(vacanciesButtonClickListener);

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		companyID = extras.getString(Constant.EXTRA_COMPANY_ID);

		content = (RelativeLayout) findViewById(R.id.activity_about_company_content);
		content.setVisibility(View.INVISIBLE);
		loadingMessage = (RelativeLayout) findViewById(R.id.activity_about_company_loading_message);

		initActionBar("Загрузка");
		
		if (NetworkManager.isNetworkConnectionAvailable(this)) {
			new CompanyParserThread().execute();
		} else {
			if (DBOperations.getCompanyInfoById(this, companyID) != null) {
				updateInformationOnScreen();
			} else {
				CustomToast.getErrorToast(this, CONNECTION_FAILED_MESSAGE,
						Toast.LENGTH_LONG).show();
			}
		}
	}

	void initActionBar(String title) {
		actionBar = getSupportActionBar();
		actionBar.setCustomView(R.layout.custom_action_bar_view);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setBackgroundDrawable(getResources().getDrawable(
				R.color.main_color));

		TextView actionBarTitle = (TextView) findViewById(R.id.custom_action_bar_view_title);
		actionBarTitle.setSelected(true);
		actionBarTitle.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
		actionBarTitle.setText(title);
		Animation leftToRightTranslate = AnimationUtils.loadAnimation(this,
				R.anim.action_bar_title_anim); // Animation translate
		actionBarTitle.startAnimation(leftToRightTranslate);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.about_company, menu);
		return true;
	}

	class CompanyParserThread extends AsyncTask<Void, Void, String> {

		boolean connectionFailedError = false;
		boolean JSONError = false;
		boolean illegalStateError = false;

		@Override
		protected void onPreExecute() {
			content.setVisibility(View.INVISIBLE);
			loadingMessage.setVisibility(View.VISIBLE);
			super.onPreExecute();
		}

		InputStream inputStream = null;
		private HttpClient client;
		private HttpGet httpGet;
		private BufferedReader in;

		@Override
		protected String doInBackground(Void... params) {
			client = PrCyClient.getInstance().client;
			httpGet = new HttpGet(Constant.API_DEV_BY_COMPANY_BY_ID_URL
					+ companyID + ".json");
			try {
				HttpResponse response = client.execute(httpGet);
				in = new BufferedReader(new InputStreamReader(response
						.getEntity().getContent()));
				StringBuffer sb = new StringBuffer("");
				String line = "";
				while ((line = in.readLine()) != null) {
					sb.append(line + "\n");
				}
				in.close();
				String result = sb.toString();
				JSONObject company = new JSONObject(result)
						.getJSONObject("company");

				JSONArray jobs = company.getJSONArray("jobs");
				ArrayList<HashMap<String, String>> vacanciesList = new ArrayList<HashMap<String, String>>();

				// TODO Сохранить вакансии в базу!
				for (int i = 0; i < jobs.length(); i++) {
					JSONObject job = jobs.getJSONObject(i).getJSONObject("job");
					HashMap<String, String> vacancy = new HashMap<String, String>();
					vacancy.put(DBConstant.ABOUT_VACANCIES_ID,
							String.valueOf(job.getInt(DBConstant.VACANCY_ID)));
					vacancy.put(DBConstant.VACANCY_TITLE,
							job.getString(DBConstant.VACANCY_TITLE));
					vacancy.put(DBConstant.ABOUT_VACANCIES_OVERVIEW,
							job.getString(DBConstant.ABOUT_VACANCIES_OVERVIEW));
					vacancy.put(DBConstant.ABOUT_VACANCIES_SALARY,
							job.getString(DBConstant.ABOUT_VACANCIES_SALARY));
					vacancy.put(DBConstant.ABOUT_VACANCIES_OPENED_COMPANY_ID,
							company.getString("id"));
					vacancy.put(DBConstant.ABOUT_VACANCIES_LAST_UPDATE_DATE,
							DateUtils.formatDateTime(AboutCompanyActivity.this,
									System.currentTimeMillis(),
									DateUtils.FORMAT_SHOW_TIME
											| DateUtils.FORMAT_SHOW_DATE
											| DateUtils.FORMAT_ABBREV_ALL));
					vacanciesList.add(vacancy);
				}

				ContentValues cv = new ContentValues();
				cv.put(DBConstant.ABOUT_COMPANIES_ID, company.getString("id"));
				cv.put(DBConstant.ABOUT_COMPANIES_TITLE,
						company.getString("title"));
				cv.put(DBConstant.ABOUT_COMPANIES_REALNAME,
						company.getString("realname"));
				cv.put(DBConstant.ABOUT_COMPANIES_SUBDOMAIN,
						company.getString("subdomain"));
				cv.put(DBConstant.ABOUT_COMPANIES_SUMMARY_RATING,
						company.getString("summary_rating"));
				cv.put(DBConstant.ABOUT_COMPANIES_OVERVIEW,
						company.getString("overview"));
				cv.put(DBConstant.ABOUT_COMPANIES_VACANCIES_OPENED,
						String.valueOf(jobs.length()));
				cv.put(DBConstant.ABOUT_COMPANIES_LAST_UPDATE_DATE, DateUtils
						.formatDateTime(AboutCompanyActivity.this,
								System.currentTimeMillis(),
								DateUtils.FORMAT_SHOW_TIME
										| DateUtils.FORMAT_SHOW_DATE
										| DateUtils.FORMAT_ABBREV_ALL));

				DBOperations.saveInfoAboutCompany(AboutCompanyActivity.this,
						cv, companyID);
				DBOperations.saveInfoAboutVacancies(AboutCompanyActivity.this,
						vacanciesList);

			} catch (JSONException e) {
				JSONError = true;
				e.printStackTrace();
			} catch (IOException e) {
				connectionFailedError = true;
				e.printStackTrace();
			} catch (IllegalStateException e) {
				illegalStateError = true;
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			if (connectionFailedError) {
				loadingMessage.setVisibility(View.INVISIBLE);
			} else {
				updateInformationOnScreen();
				content.setVisibility(View.VISIBLE);
			
				loadingMessage.setVisibility(View.INVISIBLE);
			}
		}
	}

	private void updateInformationOnScreen() {
		final HashMap<String, String> data = DBOperations.getCompanyInfoById(this,
				companyID);

		// Если соединение ОТСУТВУЕТ то показываем текст с датой последней
		// синхр.
		if (!NetworkManager.isNetworkConnectionAvailable(this)) {
			lastUpdateDate.setVisibility(View.VISIBLE);
			lastUpdateDate.setText(LAST_UPDATE_MESSAGE
					+ data.get(DBConstant.ABOUT_COMPANIES_LAST_UPDATE_DATE));
		} else { // СОЕДИНЕНО. Дату скрываем
			lastUpdateDate.setVisibility(View.GONE);
		}
		overview.setText(Html
				.fromHtml(data.get(DBConstant.ABOUT_COMPANIES_OVERVIEW))
				.toString().trim());

		if (DBConstant.ABOUT_COMPANIES_SUMMARY_RATING.compareTo("null") == 1) {
			rating.setText(data.get(DBConstant.ABOUT_COMPANIES_SUMMARY_RATING)
					+ "/5");
		} else {
			rating.setText("Нет данных");
		}

		int openedVacancies = Integer.parseInt(data
				.get(DBConstant.ABOUT_COMPANIES_VACANCIES_OPENED));

		if (openedVacancies == 0) {
			vacanciesButton.setEnabled(false);
		} else {
			vacanciesButton.setEnabled(true);
		}
		numberOfOpenedVacancies.setText(String.valueOf(openedVacancies));
		content.setVisibility(View.VISIBLE);
		loadingMessage.setVisibility(View.INVISIBLE);
		initActionBar(data.get(DBConstant.ABOUT_COMPANIES_TITLE));
		onPageButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri
						.parse("http://companies.dev.by/" + data.get(DBConstant.ABOUT_COMPANIES_SUBDOMAIN)));
				startActivity(intent);
			}
		});

	}

	OnClickListener vacanciesButtonClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			startActivity(new Intent(AboutCompanyActivity.this,
					VacanciesListActivity.class).putExtra(
					Constant.EXTRA_COMPANY_VACANCIES_LIST, DBOperations
							.getAllCompanyVacancies(AboutCompanyActivity.this,
									companyID)));
		}
	};
}
