package com.dubkovapps.devby;

import android.os.Bundle;
import android.view.Menu;

public class PreferenceActivity extends android.preference.PreferenceActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		 addPreferencesFromResource(R.xml.pref);
	}


}
