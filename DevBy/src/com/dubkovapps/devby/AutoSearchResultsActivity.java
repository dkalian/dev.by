package com.dubkovapps.devby;

import com.dubkovapps.devby.controller.activities.AboutVacancyActivity;
import com.dubkovapps.devby.controller.activities.VacanciesListActivity;
import com.dubkovapps.devby.model.adapter.VacanciesAdapter;
import com.dubkovapps.devby.model.constant.Constant;
import com.dubkovapps.devby.model.dboperation.DBOperations;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

public class AutoSearchResultsActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_auto_search_results);
		
		ActionBar actionBar = getSupportActionBar();
		actionBar.setCustomView(R.layout.custom_action_bar_view);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setBackgroundDrawable(getResources().getDrawable(
				R.color.main_color));

		TextView actionBarTitle = (TextView) findViewById(R.id.custom_action_bar_view_title);
		actionBarTitle.setSelected(true);
		actionBarTitle.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
		actionBarTitle.setText("Результат автопоиска");
		Animation leftToRightTranslate = AnimationUtils.loadAnimation(this,
				R.anim.action_bar_title_anim); // Animation translate
		actionBarTitle.startAnimation(leftToRightTranslate);
		
		ListView vacanciesListView = (ListView) findViewById(R.id.activity_auto_search_results);
		final VacanciesAdapter adapter = new VacanciesAdapter(this, DBOperations.getNewVacancies(this));
		vacanciesListView.setAdapter(adapter);
		vacanciesListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long id) {
				startActivity(new Intent(AutoSearchResultsActivity.this ,AboutVacancyActivity.class).putExtra(Constant.EXTRA_VACANCY_DATA, adapter.getItem(pos)));
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.auto_search_results, menu);
		return true;
	}
}
